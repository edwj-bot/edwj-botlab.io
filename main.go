package main

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"os"
	"strings"
	"time"

	"github.com/GeertJohan/go.rice"
	"golang.org/x/net/html"
)

type TemplateFiller struct {
	Date string
	HTML template.HTML
}

const codeTemplateString = `
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<title>Every Day With Jesus - {{.Date}}</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="edwjbot-container">
			{{.HTML}}
		</div>
	</body>
</html>
`

type node_matcher func(node *html.Node) (match bool)

func firstNodeMatchingFunc(rootNode *html.Node, matcher node_matcher) (*html.Node, error) {
	if matcher(rootNode) {
		return rootNode, nil
	}

	for sub := rootNode.FirstChild; sub != nil; sub = sub.NextSibling {
		foundNode, _ := firstNodeMatchingFunc(sub, matcher)
		if foundNode != nil {
			return foundNode, nil
		}
	}
	return nil, errors.New("Could not find the node.")
}

func getRootNodeForMonth(month string) (*html.Node, error) {
	reader, err := rice.MustFindBox("databox").Open("Every Day With Jesus - " + month + ".xhtml")
	if err != nil {
		return nil, err
	}
	rootNode, err := html.Parse(reader)
	if err != nil {
		return nil, err
	}
	return rootNode, nil
}

func getNodeForDateString(date string) (*html.Node, error) {
	monthNode, err := getRootNodeForMonth(strings.Split(date, " ")[0])
	if err != nil {
		return nil, err
	}
	return firstNodeMatchingFunc(monthNode, func(node *html.Node) bool {
		for _, attr := range node.Attr {
			if attr.Key == "class" && attr.Val == "Date-Black" {
				if node.FirstChild.Data == date {
					return true
				}
			}
		}
		return false
	})
}

func correctFormattingOfSingleNode(node *html.Node) {
	// Remove extra space between the preceding tag and this one, so it is not split up
	prev := node.PrevSibling
	if prev != nil && prev.Type == html.TextNode {
		prev.Data = strings.TrimRight(prev.Data, " \t\n")
	}
	// Add an extra space after the node if another word is coming up, so they are not squished together
	next := node.NextSibling
	if next != nil && next.Type == html.TextNode {
		alphabet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		if len(next.Data) > 0 && strings.ContainsRune(alphabet, []rune(next.Data)[0]) {
			next.Data = " " + next.Data
		}
	}
	// Note that the node is fixed so we don't try it again
	node.Attr = append(node.Attr, html.Attribute{
		Key: "data-is-fixed-formatting",
		Val: "yes",
	})
}

func correctFormattingOfRootNode(rootNode *html.Node) {
	doesNodeNeedCorrection := func(node *html.Node) bool {
		needsCorrection := false
		for _, attr := range node.Attr {
			// These are the nodes that need spacing corrections.
			if attr.Key == "class" && (strings.HasPrefix(attr.Val, "hashem") || attr.Val == "Head-main") {
				needsCorrection = true
			}
			if attr.Key == "data-is-fixed-formatting" && attr.Val == "yes" {
				return false
			}
		}
		return needsCorrection
	}
	var recurse func(node *html.Node)
	recurse = func(node *html.Node) {
		if node == nil {
			return
		}
		if doesNodeNeedCorrection(node) {
			correctFormattingOfSingleNode(node)
		}
		for sub := node.FirstChild; sub != nil; sub = sub.NextSibling {
			recurse(sub)
		}
	}
	recurse(rootNode)
}

func getHTMLTextForDateString(date string) (string, error) {
	firstNode, err := getNodeForDateString(date)

	if err != nil {
		return "", err
	}
	buf := bytes.NewBufferString("")
	writtenNode := firstNode
writingLoop:
	for {
		if writtenNode == nil {
			// The last day of the month.
			break writingLoop
		}
		if writtenNode != firstNode {
			for _, attr := range writtenNode.Attr {
				if attr.Key == "class" && attr.Val == "Date-Black" {
					break writingLoop
				}
			}
		}
		correctFormattingOfRootNode(writtenNode)
		err = html.Render(buf, writtenNode)
		if err != nil {
			return "", err
		}
		writtenNode = writtenNode.NextSibling
	}
	return buf.String(), nil
}

func templateFill(tf TemplateFiller) string {
	tmpl, _ := template.New("page").Parse(codeTemplateString)
	buf := bytes.NewBufferString("")
	tmpl.Execute(buf, tf)
	return buf.String()
}

func getAllDays() []string {
	days := []string{}
	// 2013 is known to be not a leap year, and there is no February 29th on this, so it works just fine
	t := time.Date(2013, time.January, 1, 8, 0, 0, 0, time.UTC)
	oneday, err := time.ParseDuration("24h")
	if err != nil {
		panic(err)
	}
	for t.Year() == 2013 {
		days = append(days, t.Format("January 2"))
		t = t.Add(oneday)
	}
	return days
}

func printStatus(percent *float32, done chan bool) {
	p := func() {
		fmt.Fprintf(os.Stderr, "%5.1f%%\n", *percent)
	}
	p()
	for {
		timeout := make(chan bool, 1)
		go func() {
			time.Sleep(1 * time.Second)
			timeout <- true
		}()
		select {
		case <-done:
			p()
			return
		case <-timeout:
			p()
		}
	}
}

func makeZip() {
	var progress float32 = 0
	done := make(chan bool, 1)

	go printStatus(&progress, done)
	zipWriter := zip.NewWriter(os.Stdout)
	tmpl, _ := template.New("page").Parse(codeTemplateString)
	days := getAllDays()
	for index, day := range days {
		dayWithHyphen := strings.Join(strings.Split(day, " "), "-")
		fileWriter, err := zipWriter.Create(dayWithHyphen + ".html")
		if err != nil {
			panic(err)
		}
		html, err := getHTMLTextForDateString(day)
		progress = 100.0 * float32(index+1) / float32(len(days))
		if err != nil {
			panic(err)
		}
		filler := TemplateFiller{
			Date: day,
			HTML: template.HTML(html),
		}
		tmpl.Execute(fileWriter, filler)
		if err != nil {
			panic(err)
		}
	}
	done <- true

	assetsBox := rice.MustFindBox("assets")
	assetsBox.Walk("/", func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			fileWriter, err := zipWriter.Create(info.Name())
			fileReader, err := assetsBox.Open(path)
			if err != nil {
				return err
			}
			io.Copy(fileWriter, fileReader)
		}
		return nil
	})
	zipWriter.Close()
}

func main() {
	makeZip()
}
